<?php

namespace shisou\tpinit;

/**
 * 七牛云图片处理
 * @param string $imgCover 要处理的图片
 * @param string $size     宽x高
 * @return string  返回类型
 * @version 20220723
 */
function image($imgCover = '', $size = '')
{
    if (!$imgCover) {
        $imgCover = 'http://qn1.10soo.net/art/202209211128389427';
    }

    $wh = $size ? explode('x', $size) : '';

    $cutting = $wh ? '?imageView2/1/w/' . $wh[0] . '/h/' . $wh[1] . '/q/75' : '';

    return $imgCover . $cutting;
}

/**
 * 分页查询
 * @param string $model ModelName
 * @param array  $where 条件
 * @param string $order 排序
 * @param int    $limit 每页数量
 * @return object 返回类型
 * @version 20220726
 */
function pageList($model, $where = [], $order = null, $limit = 6)
{
    $where[] = [
        'status',
        '=',
        1,
    ];

    $order = is_null($order) ? 'id desc' : $order;

    $model = "\\app\\model\\" . $model;

    $list = (new $model)->filterWhere($where)
        ->order($order)
        ->paginate([
            'list_rows' => $limit,
            'var_page'  => 'p',
        ]);

    return $list;
}

/**
 * 详情页URL
 * @param array $param ['id'=>1,'title'='']
 * @return string  返回类型
 * @version 20220726
 */
function show($param = [])
{
    return url('public/details', $param);
}

/**
 * 栏目URL
 * @param int $cateId
 * @return string  返回类型
 * @version 20220726
 */
function cateUrl($cateId = 0)
{
    $cate = \app\model\Cate::findOrNew($cateId);

    return $cate->recUrl();
}

/**
 * 判断微信客户端
 * @version 20220430
 */
function isWx()
{
    return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false;
}

/**
 * 判断移动端
 * @version 20220820
 */
function isMobile()
{
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
        return true;
    }

    // 此条摘自TPM智能切换模板引擎，适合TPM开发
    if (isset($_SERVER['HTTP_CLIENT']) && 'PhoneClient' == $_SERVER['HTTP_CLIENT']) {
        return true;
    }

    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset($_SERVER['HTTP_VIA'])) {
        return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
    }
    // 判断手机发送的客户端标志,兼容性有待提高
    if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = [
            'nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp', 'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu', 'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile',
        ];
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
    }
    // 协议法，因为有可能不准确，放到最后判断
    if (isset($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return true;
        }
    }

    return false;
}