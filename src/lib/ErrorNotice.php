<?php

namespace shisou\tpinit\lib;


class ErrorNotice
{
    private $content;

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function send()
    {
        $contacts = config('local.contacts');

        try {
            if ($contacts['emails']) {
                (new Email())->sendEmail($contacts['emails'], $this->content);
            }

            if ($contacts['mobiles']) {
                foreach ($contacts['mobiles'] as $value) {
                    (new Sms)->sendCode($value, $this->content);
                }
            }
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
