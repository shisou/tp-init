<?php

namespace shisou\tpinit\lib;

use \PHPMailer\PHPMailer\PHPMailer;

class Email
{
    public static $err = '';

    public function sendEmail($contacts, $content)
    {
        $mail = new PHPMailer();
        try {
            $mail->SMTPDebug = 0;   // 邮件调试模式
            $mail->isSMTP();        // 设置邮件使用SMTP
            $mail->SMTPAuth = true; // smtp需要鉴权 这个必须是true

            $mail->Host       = 'ssl://smtp.exmail.qq.com';  // 设置邮件程序以使用SMTP
            $mail->SMTPSecure = 'ssl';      // 设置使用ssl加密方式登录鉴权   企业邮箱必须关闭,个人邮箱才需要
            $mail->Port       = 465;       // 企业邮箱服务器端口号

            $mail->CharSet  = 'UTF-8';            // 设置邮件内容的编码
            $mail->Username = 'admin@10sw.com';   // SMTP username
            $mail->Password = '8RUMS9JuFVw3dc5A'; // SMTP password

            $mail->setFrom('admin@10sw.com', '系统报错');  //设置发件人

            foreach ($contacts as $val) {
                if (!empty($val)) {
                    $mail->addAddress($val);      //  添加收件人1
                }
            }
            // $mail->addReplyTo('admin@10sw.com', '到期提醒');  //收件人回复的邮箱

            $mail->isHTML(true);   // 将电子邮件格式设置为HTML

            $mail->Subject = config('local.website') . '系统报错';
            $mail->Body    = $content;
            // $mail->AltBody = '这是非HTML邮件客户端的纯文本';

            if (!$mail->send()) {
                static::$err = $mail->ErrorInfo;
                return false;
            }
        } catch (\Throwable $th) {
            static::$err = $th->getMessage();
            return false;
        }
        return true;
    }
}
