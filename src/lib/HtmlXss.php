<?php

namespace shisou\tpinit\lib;

class HtmlXss
{
    public function stringPreg($string)
    {
        $str = strtolower($string);

        if (strstr($str, 'onmouseover')) {
            $data = str_replace('onmouseover', "", $str);

            return $this->stringPreg($data);
        }
        if (strstr($str, 'style')) {
            $data = str_replace('style', "", $str);

            return $this->stringPreg($data);
        }
        if (strstr($str, 'alert')) {
            $data = str_replace('alert', "", $str);

            return $this->stringPreg($data);
        }
        if (strstr($str, 'script')) {
            $data = str_replace('script', "", $str);

            return $this->stringPreg($data);
        }
        if (strstr($str, 'window')) {
            $data = str_replace('window', "", $str);

            return $this->stringPreg($data);
        }
        if (strstr($str, 'location')) {
            $data = str_replace('location', "", $str);

            return $this->stringPreg($data);
        }
        if (strstr($str, 'onclick')) {
            $data = str_replace('onclick', "", $str);

            return $this->stringPreg($data);
        }
        if (strstr($str, '">')) {
            $data = str_replace('">', "", $str);

            return $this->stringPreg($data);
        }
        if (strstr($str, "'>")) {
            $data = str_replace("'>", "", $str);

            return $this->stringPreg($data);
        }
        if (strpos($str, 'on')) {
            $patterns = '/\son\w+="[^"]*"/i';
            $data     = preg_replace($patterns, '', $str);

            return str_replace($data, "", $str);
        }

        return $str;
    }
}