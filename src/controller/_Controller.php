<?php
declare(strict_types=1);

namespace shisou\tpinit\controller;

use think\App;
use think\exception\ValidateException;
use think\Validate;
use think\helper\Str;

abstract class _Controller
{
    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    protected $globals = [];

    protected $reqId = 0;

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app)
    {
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

        $this->app     = $app;
        $this->request = $this->app->request;

        define('DATETIME', date('Y-m-d H:i:s'));

        define('__CONTROLLER__', str_replace('.', '/', Str::lower($this->request->controller())));

        define('__ACTION__', Str::lower($this->request->action()));

        define('__URL__', __CONTROLLER__ . '/' . __ACTION__);

        define('__ALIAS__', $this->request->rule()
            ->getRule() ?: '/');

        if (!($this->reqId = $this->request->post('id'))) {
            $this->reqId = $this->request->get('id');
        }
    }


    /**
     * 验证数据
     * @access protected
     * @param array        $data     数据
     * @param string|array $validate 验证器名或者验证规则数组
     * @param array        $message  提示信息
     * @param bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [
                    $validate,
                    $scene,
                ] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)
            ->check($data);
    }

    protected function response($code, $msg, $data, $url)
    {
        $json = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data,
            'url'  => $url,
        ];

        return json($json);
    }

    /**
     * 操作成功返回
     *
     * ```
     * // 直接返回成功
     * $this->success();
     * // 只返回信息
     * $this->success($msg);
     * // 只返回数据
     * $this->success($array);
     * ```
     *
     * @param array|string $msg
     * @param              $data
     * @param              $url
     * @return json
     */
    protected function success($msg = '', $data = null, $url = '')
    {
        if (is_array($msg)) {
            $data = $msg;
            $msg  = '';
        }

        //        $msg = $msg ?: '成功';

        return $this->response(1, $msg, $data, $url);
    }

    /**
     * 操作失败返回
     * @param $msg
     * @param $data
     * @param $url
     * @return json
     */
    protected function fail($msg, $data = null, $url = '')
    {
        return $this->response(0, $msg, $data, $url);
    }
}