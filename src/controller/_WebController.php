<?php

namespace shisou\tpinit\controller;

use app\model\User;
use think\facade\Request;
use think\facade\Route;
use think\Exception;
use think\App;

class _WebController extends _Controller
{
    /**
     * @var User
     */
    public $user = null;

    protected $layout = '_web';

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->globals['setting']   = \app\model\Setting::select();

        // ------------------------------
        // 用户登录
        // ------------------------------
        $this->user = session('user');
    }

    /**
     * @param $view
     * @param $param
     * @return \think\response\View
     */
    public function view($view = null, $param = null)
    {
        if (is_array($view) || !$view) {
            $param = $view;

            $view = $view ?: $this->request->action();
        }

        $arr = [
            'hideNav' => false,
        ];

        if ($param) {
            $arr = array_merge($arr, $param);
        }
        $param = array_merge($arr, $this->globals);

        if (!isset($param['layout'])) {
            $this->app->view->layout('_web');
        }

        return view($view, $param);
    }
}
