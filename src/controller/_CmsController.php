<?php

namespace shisou\tpinit\controller;

use app\model\Cate;
use app\model\User;
use think\facade\Request;
use think\facade\Route;
use think\Exception;
use think\App;

class _CmsController extends _Controller
{
    /**
     * @var Cate
     */
    protected $curCate;

    /**
     * @var User
     */
    public $user = null;

    protected $layout = '_web';

    public function __construct(App $app)
    {
        parent::__construct($app);

        $curCate                 = \app\model\Cate::filterWhere(['alias' => __ALIAS__])
            ->find();
        $this->curCate           = $curCate;
        $this->globals['crumbs'] = \app\model\Cate::getParentList($curCate);

        define('__CURCATEID__', $curCate->id);
        define('__PROCATEID__', $this->request->get('cate_id') ?? $curCate->children[0]['id']);

        $this->globals['setting'] = \app\model\Setting::select();
        $this->globals['cates']   = \app\model\Cate::filterWhere([
            'status'    => 1,
            'parent_id' => 0,
            'is_nav'    => 1,
        ])
            ->order('sort desc,id asc')
            ->select();

        $cateModel                  = new \app\model\Cate;
        $this->globals['cateModel'] = $cateModel;

        // ------------------------------
        // 用户登录
        // ------------------------------
        $this->user = session('user');
    }

    /**
     * @param $view
     * @param $param
     * @return \think\response\View
     */
    public function view($view = null, $param = null)
    {
        if (is_array($view) || !$view) {
            $param = $view;

            $curCate = $this->curCate;

            $view = $curCate['tpl_page'] ?: $curCate['tpl_list'];

            if (empty($view) && __ALIAS__ != 'show') {
                throw new Exception('模板' . $view . '未设置');
            }

            $view = $view ?: $this->request->action();
        }

        $arr = [
            'hideNav' => false,
        ];

        $this->globals['curCate'] = $this->curCate;

        if ($param) {
            $arr = array_merge($arr, $param);
        }
        $param = array_merge($arr, $this->globals);

        if (!isset($param['layout'])) {
            $this->app->view->layout('_web');
        }

        return view($view, $param);
    }
}
